0.0.9

* Use aeson-1.0.
* Add nix build.

0.0.8

* Handle `"GPS_solution":"DGPS (SBAS / WAAS)"`

0.0.7

* Fix bug with ConstrainedClassMethods in type-classes.

0.0.6

* Create `SettingsSet` data type.
* Create `Situation` data type.
* Split out some type-classes to own module.
* Organise tests better.

0.0.5

* Create `Settings` data type.

0.0.4

* Create `Status` data type.

0.0.3

* Move `stratux` to `stratux-types`.

0.0.1

* Initial.

