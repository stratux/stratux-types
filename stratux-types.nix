{ mkDerivation, aeson, base, bytestring, lens, scientific, stdenv
, text, time
}:
mkDerivation {
  pname = "stratux-types";
  version = "0.0.12";
  src = ./.;
  libraryHaskellDepends = [
    aeson base bytestring lens scientific text time
  ];
  homepage = "https://gitlab.com/stratux/stratux-types";
  description = "A library for reading JSON output from stratux";
  license = stdenv.lib.licenses.bsd3;
}
