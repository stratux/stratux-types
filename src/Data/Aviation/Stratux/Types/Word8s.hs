{-# LANGUAGE NoImplicitPrelude #-}

module Data.Aviation.Stratux.Types.Word8s(
  HasWord8s(..)
) where

import Control.Category(id)
import Control.Lens(Traversal')
import Data.Word(Word8)

class HasWord8s a where
  word8s ::
    Traversal'
      a
      Word8

instance HasWord8s Word8 where
  word8s =
    id
