{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}

module Data.Aviation.Stratux.Types.TargetType(
  TargetType(..)
, AsTargetType(..)
) where

import Control.Lens(makeClassyPrisms, Prism', prism', (^?), ( # ))
import Control.Monad(Monad(return), mzero)
import Data.Aeson(FromJSON(parseJSON), ToJSON(toJSON), Value(Number), withScientific)
import Data.Eq(Eq)
import Data.Int(Int)
import Data.Maybe
import Data.Ord(Ord)
import Data.Scientific(Scientific)
import Data.Word(Word)
import Prelude(Num, Show, Integer, Float, Double)

-- $setup
-- >>> :set -XOverloadedStrings
-- >>> import Control.Lens
-- >>> import Data.Aeson(decode, encode)
-- >>> import Data.Maybe(Maybe)
-- >>> import Prelude

data TargetType =
  ModeS
  | Adsb
  | Adsr
  | TisbS
  | Tisb
  deriving (Eq, Ord, Show)

makeClassyPrisms ''TargetType

-- |
--
-- >>> _TargetType # ModeS :: Scientific
-- 0.0
--
-- >>> _TargetType # Adsb :: Scientific
-- 1.0
--
-- >>> _TargetType # Tisb :: Scientific
-- 4.0
instance AsTargetType Scientific where
  _TargetType =
    targetTypeNum

-- |
--
-- >>> _TargetType # ModeS :: Int
-- 0
--
-- >>> _TargetType # Adsb :: Int
-- 1
--
-- >>> _TargetType # Tisb :: Int
-- 4
instance AsTargetType Int where
  _TargetType =
    targetTypeNum

-- |
--
-- >>> _TargetType # ModeS :: Integer
-- 0
--
-- >>> _TargetType # Adsb :: Integer
-- 1
--
-- >>> _TargetType # Tisb :: Integer
-- 4
instance AsTargetType Integer where
  _TargetType =
    targetTypeNum

-- |
--
-- >>> _TargetType # ModeS :: Float
-- 0.0
--
-- >>> _TargetType # Adsb :: Float
-- 1.0
--
-- >>> _TargetType # Tisb :: Float
-- 4.0
instance AsTargetType Float where
  _TargetType =
    targetTypeNum

-- |
--
-- >>> _TargetType # ModeS :: Double
-- 0.0
--
-- >>> _TargetType # Adsb :: Double
-- 1.0
--
-- >>> _TargetType # Tisb :: Double
-- 4.0
instance AsTargetType Double where
  _TargetType =
    targetTypeNum

-- |
--
-- >>> _TargetType # ModeS :: Word
-- 0
--
-- >>> _TargetType # Adsb :: Word
-- 1
--
-- >>> _TargetType # Tisb :: Word
-- 4
instance AsTargetType Word where
  _TargetType =
    targetTypeNum

targetTypeNum ::
  (Num a, Eq a) =>
  Prism'
    a
    TargetType
targetTypeNum =
  prism'
    (\t ->  case t of
              ModeS ->
                0
              Adsb ->
                1
              Adsr ->
                2
              TisbS ->
                3
              Tisb ->
                4) 
    (\n ->  case n of
              0 ->
                Just ModeS
              1 ->
                Just Adsb
              2 ->
                Just Adsr
              3 -> 
                Just TisbS
              4 ->
                Just Tisb
              _ ->
                Nothing)

-- |
--
-- >>> decode "0" :: Maybe TargetType
-- Just ModeS
--
-- >>> decode "1" :: Maybe TargetType
-- Just Adsb
--
-- >>> decode "4" :: Maybe TargetType
-- Just Tisb
--
-- >>> decode "5" :: Maybe TargetType
-- Nothing
instance FromJSON TargetType where
  parseJSON =
    withScientific "TargetType" (\n -> case n ^? targetTypeNum of
      Nothing ->
        mzero
      Just t ->
        return t)

-- |
--
-- >>> encode ModeS
-- "0"
--
-- >>> encode Adsb
-- "1"
--
-- >>> encode Tisb
-- "4"
instance ToJSON TargetType where
  toJSON c =
    Number (targetTypeNum # c)
