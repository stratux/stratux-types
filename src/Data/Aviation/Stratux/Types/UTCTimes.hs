{-# LANGUAGE NoImplicitPrelude #-}

module Data.Aviation.Stratux.Types.UTCTimes(
  HasUTCTimes(..)
) where

import Control.Category(id)
import Control.Lens(Traversal')
import Data.Time(UTCTime)

-- $setup
-- >>> :set -XOverloadedStrings
-- >>> import Control.Lens
-- >>> import Data.Time

class HasUTCTimes a where
  utcTimes ::
    Traversal' 
      a
      UTCTime

-- |
--
-- >>> (utcTimes %~ addUTCTime 1) (UTCTime (fromGregorian 1 1 1) 600)
-- 0001-01-01 00:10:01 UTC
instance HasUTCTimes UTCTime where
  utcTimes =
    id
